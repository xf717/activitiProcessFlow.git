package com.gaul.activiti;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.junit.Test;

/**
 * 连线
 */
public class SequenceFlowManager {
private ProcessEngine engine = ProcessEngineFactory.createProcessEngine();
	
	@Test
	public void deploy() {
//		ProcessEngine engine = ProcessEngineFactory.createProcessEngine();
		//获取仓库服务，管理流程定义
		RepositoryService repositoryService = engine.getRepositoryService();
		Deployment deploy = repositoryService.createDeployment()
								.addClasspathResource("diagrams/SequenceFlow.bpmn")  //从类路径中添加资源，一次只能添加一个资源
								.addClasspathResource("diagrams/SequenceFlow.png")
								.name("连线流程")  //设置部署的名称
								.category("办公")  //设置部署的类别
								.deploy();
		System.out.println("部署的id：" + deploy.getId());
		System.out.println("部署的名称：" + deploy.getName());
	}
	
	@Test
	public void startProcess() {
		String processDefiKey = "sequenceBill";
		RuntimeService service = engine.getRuntimeService();
		ProcessInstance instance = service.startProcessInstanceByKey(processDefiKey);//通过流程定义的key来执行流程
		System.out.println("流程实例id：" + instance.getProcessInstanceId());
		System.out.println("流程定义id：" + instance.getProcessDefinitionId());
	}
	
	@Test
	public void queryTask() {
		//任务的办理人
//		String assignee = "李四";
		//取得任务服务
		TaskService service = engine.getTaskService();
		//创建任务查询对象
		TaskQuery query = service.createTaskQuery();
//		List<Task> list = query.taskAssignee(assignee).list();//指定办理人任务列表
		List<Task> list = query.list();
		if(list != null && list.size() > 0) {
			for(Task task : list) {
				System.out.println("任务的办理人：" + task.getAssignee());
				System.out.println("任务的id：" + task.getId());
				System.out.println("任务的名称：" + task.getName());
			}
		}
	}
	
	@Test
	public void compileTask() {
		String taskId = "904";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("message", "重要");
		params.put("assignee", "boss");
		//taskId：任务id
		engine.getTaskService().complete(taskId, params);
		System.out.println("任务执行完毕 compileTask");
	}
	
	@Test
	public void setVariable() {
		String taskId = "904";
		TaskService service = engine.getTaskService();
		
		//采用TaskService来设置流程变量
		service.setVariable(taskId, "message", "重要");//设置单一的变量
		service.setVariable(taskId, "assignee", "boss");
	}
	
	@Test
	public void getVariable() {
		String taskId = "904";
		TaskService service = engine.getTaskService();
		System.out.println(service.getVariable(taskId, "message"));
	}
}
