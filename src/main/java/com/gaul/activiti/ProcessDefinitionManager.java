package com.gaul.activiti;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

/**
 * 流程定义
 */
public class ProcessDefinitionManager {
	private ProcessEngine engine = ProcessEngineFactory.createProcessEngine();
	
	/**
	 * act_re_prodef 流程定义表
	 * act_re_deployment 部署表
	 * act_ge_property 通用属性表，id生成策略
	 */
	public void deploy() {
//		ProcessEngine engine = ProcessEngineFactory.createProcessEngine();
		//获取仓库服务，管理流程定义
		RepositoryService repositoryService = engine.getRepositoryService();
		Deployment deploy = repositoryService.createDeployment()
								.addClasspathResource("diagrams/LeaveBill.bpmn")  //从类路径中添加资源，一次只能添加一个资源
								.addClasspathResource("diagrams/LeaveBill.png")
								.name("请假单流程")  //设置部署的名称
								.category("办公")  //设置部署的类别
								.deploy();
		System.out.println("部署的id：" + deploy.getId());
		System.out.println("部署的名称：" + deploy.getName());
	}
	
	public void viewImage() {
		String deploymentId = "1";
		String imageName = null;
		//取得某个部署的资源的名称
		List<String> names = engine.getRepositoryService().getDeploymentResourceNames(deploymentId);
		if(names != null && names.size() > 0) {
			for(String name : names) {
				if(name.endsWith(".png")) {
					imageName = name;
				}
			}
		}
		System.out.println(imageName);
		//读取资源
		InputStream in = engine.getRepositoryService().getResourceAsStream(deploymentId, imageName);
		//把文件输入流写入文件中
		File file = new File("d:/" + imageName);
		try {
			FileUtils.copyInputStreamToFile(in, file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteProcessDef() {
		//通过部署id来删除流程定义
		String deploymentId = "1";
		engine.getRepositoryService().deleteDeployment(deploymentId);
	}
}
