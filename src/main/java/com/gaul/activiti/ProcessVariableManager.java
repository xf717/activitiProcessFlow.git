package com.gaul.activiti;

import java.util.Date;
import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.junit.Test;

/**
 * 流程变量
 */
public class ProcessVariableManager {
	private ProcessEngine engine = ProcessEngineFactory.createProcessEngine();
	
	@Test
	public void deploy() {
//		ProcessEngine engine = ProcessEngineFactory.createProcessEngine();
		//获取仓库服务，管理流程定义
		RepositoryService repositoryService = engine.getRepositoryService();
		Deployment deploy = repositoryService.createDeployment()
								.addClasspathResource("diagrams/AppayBill.bpmn")  //从类路径中添加资源，一次只能添加一个资源
								.addClasspathResource("diagrams/AppayBill.png")
								.name("支付流程")  //设置部署的名称
								.category("办公")  //设置部署的类别
								.deploy();
		System.out.println("部署的id：" + deploy.getId());
		System.out.println("部署的名称：" + deploy.getName());
	}
	
	@Test
	public void startProcess() {
		String processDefiKey = "appayBill";
		RuntimeService service = engine.getRuntimeService();
		ProcessInstance instance = service.startProcessInstanceByKey(processDefiKey);//通过流程定义的key来执行流程
		System.out.println("流程实例id：" + instance.getProcessInstanceId());
		System.out.println("流程定义id：" + instance.getProcessDefinitionId());
	}
	
	@Test
	public void compileTask() {
		String taskId = "104";
		//taskId：任务id
		engine.getTaskService().complete(taskId);
		System.out.println("任务执行完毕 compileTask");
	}
	
	@Test
	public void queryTask() {
		//任务的办理人
//		String assignee = "李四";
		//取得任务服务
		TaskService service = engine.getTaskService();
		//创建任务查询对象
		TaskQuery query = service.createTaskQuery();
//		List<Task> list = query.taskAssignee(assignee).list();//指定办理人任务列表
		List<Task> list = query.list();
		if(list != null && list.size() > 0) {
			for(Task task : list) {
				System.out.println("任务的办理人：" + task.getAssignee());
				System.out.println("任务的id：" + task.getId());
				System.out.println("任务的名称：" + task.getName());
			}
		}
	}
	
	/**
	 * 设置流程变量值
	 */
	@Test
	public void setVariable() {
		String taskId = "604";
		TaskService service = engine.getTaskService();
		
		//采用TaskService来设置流程变量
//		service.setVariable(taskId, "cost", 1000);//设置单一的变量
//		service.setVariable(taskId, "申请时间", new Date());
//		service.setVariableLocal(taskId, "申请人", "张三");
		
		//自定义类型
		AppayBillBean appayBill = new AppayBillBean();
		appayBill.setId(1);
		appayBill.setCost(300);
		appayBill.setDate(new Date());
		appayBill.setAppayPerson("gao");
		service.setVariable(taskId, "appayBillBean", appayBill);
	}
	
	/**
	 * 查询流程变量
	 */
	@Test
	public void getVariable() {
		String taskId = "604";
		TaskService service = engine.getTaskService();
		System.out.println(service.getVariable(taskId, "cost"));
		System.out.println(service.getVariableLocal(taskId, "申请人"));
	}
	
	/**
	 * 流程变量设置
	 */
	@Test
	public void getAndSetProcessVariable() {
		//两种服务可以设置流程变量
//		TaskService taskService = engine.getTaskService();
//		RuntimeService runtimeService = engine.getRuntimeService();
		
		//runtimeService设置流程变量
//		runtimeService.setVariable(executionId, variableName, values);
//		runtimeService.setVariableLocal(executionId, variableName, values);//设置本执行对象的变量，该变量的作用域只在当前的execution对象有效
//		runtimeService.setVariables(executionId, variables);//可以设置多个变量，放在Map<key,value> Map<String,Object>中
		
		//taskService设置流程变量
//		taskService.setVariable(taskId, variableName, values);
//		taskService.setVariableLocal(taskId, variableName, values);
//		taskService.setVariables(taskId, variables);
		
		//当流程开始执行的时候，设置变量
//		engine.getRuntimeService().startProcessInstanceByKey(processDefKey, variables);
		
		//当执行任务时候，可以设置流程变量
//		engine.getTaskService().complete(taskId, variables);
		
		/**
		 * 通过RuntimeService取值
		 */
//		runtimeService.getVariable(executionId, variableName);
//		runtimeService.getVariableLocal(executionId, variableName);
//		runtimeService.getVariables(variablesName);//取当前执行对象的所有变量
		
		/**
		 * 通过TaskService取值
		 */
//		taskService.getVariable(taskId, variablename);
//		taskService.getVariableLocal(taskId, variableName);
//		taskService.getVariables(taskId);
	}
}
