package com.gaul.activiti;

import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.junit.Test;

/**
 * 流程实例与流程任务
 */
public class ProcessInstanceAndTaskManager {
	private ProcessEngine engine = ProcessEngineFactory.createProcessEngine();

	/**
	 * 执行流程
	 */
	public void startProcess() {
		String processDefiKey = "leaveBill";
		RuntimeService service = engine.getRuntimeService();
		ProcessInstance instance = service.startProcessInstanceByKey(processDefiKey);//通过流程定义的key来执行流程
		System.out.println("流程实例id：" + instance.getProcessInstanceId());
		System.out.println("流程定义id：" + instance.getProcessDefinitionId());
	}
	
	/**
	 * 查询任务
	 */
	@Test
	public void queryTask() {
		//任务的办理人
//		String assignee = "李四";
		//取得任务服务
		TaskService service = engine.getTaskService();
		//创建任务查询对象
		TaskQuery query = service.createTaskQuery();
//		List<Task> list = query.taskAssignee(assignee).list();//指定办理人任务列表
		List<Task> list = query.list();
		if(list != null && list.size() > 0) {
			for(Task task : list) {
				System.out.println("任务的办理人：" + task.getAssignee());
				System.out.println("任务的id：" + task.getId());
				System.out.println("任务的名称：" + task.getName());
			}
		}
	}
	
	/**
	 * 获取流程实例的状态
	 */
	@Test
	public void getProcessInstanceState() {
		String processInstanceId = "101";
		ProcessInstance result = engine.getRuntimeService()
									.createProcessInstanceQuery()
									.processInstanceId(processInstanceId)
									.singleResult();
		if(result != null) {
			System.out.println("当前活动任务id：" + result.getActivityId());
		} else {
			System.out.println("流程实例已结束");
		}
	}
	
	/**
	 * 执行任务
	 */
	@Test
	public void compileTask() {
		String taskId = "104";
		//taskId：任务id
		engine.getTaskService().complete(taskId);
		System.out.println("任务执行完毕 compileTask");
	}
	
	/**
	 * 查询历史任务流程实例
	 */
	@Test
	public void queryHistoryProcessInstance() {
		HistoryService service = engine.getHistoryService();
		List<HistoricProcessInstance> instances = service.createHistoricProcessInstanceQuery().list();
		if(instances != null && instances.size() > 0) {
			for(HistoricProcessInstance instance : instances) {
				System.out.println("历史流程实例id：" + instance.getId());
				System.out.println("历史流程实例定义id：" + instance.getProcessDefinitionId());
				System.out.println("历史流程实例结束时间：" + instance.getEndTime());
			}
		}
	}
	
	/**
	 * 查询历史流程任务
	 */
	@Test
	public void queryHistoryTask() {
		String processInstanceId = "101";
		HistoryService service = engine.getHistoryService();
		List<HistoricTaskInstance> instances = service.createHistoricTaskInstanceQuery()
													.processInstanceId(processInstanceId)
													.list();
		if(instances != null && instances.size() > 0) {
			for(HistoricTaskInstance instance : instances) {
				System.out.println("历史流程实例任务id：" + instance.getId());
				System.out.println("历史流程实例任务名称：" + instance.getName());
				System.out.println("历史流程实例处理人：" + instance.getAssignee());
			}
		}
	}
}
